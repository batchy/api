package main

import (
	"bitbucket.org/batchy/api/v1/environment"
	"bitbucket.org/batchy/api/v1/router"
)

func main() {
	err := environment.InitDB()
	if err != nil {
		panic(err.Error())
	}

	e := router.Init()
	e.Logger.Fatal(e.Start(":1323"))
}
