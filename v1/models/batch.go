package models

type Batch struct {
	ID          string  `json:"id"`
	Name        string  `json:"name"`
	Description string  `json:"description"`
	Tasks       []*Task `json:"tasks"`
}
