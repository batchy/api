package models

import "time"

type Task struct {
	ID          string    `json:"id"`
	Name        string    `json:"name"`
	Description string    `json:"description"`
	Subtasks    []Subtask `json:"subtasks"`
	Status      string    `json:"status"`
	DueDate     time.Time `json:"dueDate"`
	BatchID     string    `json:"batchId"`
	BatchName   string    `json:"batchName"`
}

type Subtask struct {
	ID   string `json:"id"`
	Body string `json:"body"`
}
