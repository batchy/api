package environment

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
)

// SourceName contains database credentials.
const SourceName = "root:april31989@tcp(db:3306)/batchy?parseTime=true"

// DB is a pointer to an SQL connection pull which can be exposed on import.
var DB *sql.DB

// Init attempts to establish a connection according to the provided SourceName.
func InitDB() error {
	fmt.Printf("Attempting to connect to database %s...\n", SourceName)
	db, err := sql.Open("mysql", SourceName)
	if err != nil {
		return err
	}
	fmt.Printf("Connection established. Pinging database...\n")
	if err := db.Ping(); err != nil {
		return err
	}
	fmt.Println("Successfully pinged database. Attaching to context and continuing...")
	DB = db
	return nil
}
