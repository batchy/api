package batch

import (
	BatchService "bitbucket.org/batchy/api/v1/services/batch"
	"bitbucket.org/batchy/api/v1/utils/auth"
	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"
	"net/http"
)

func Get(c echo.Context) error {
	userID := c.Get("user").(*jwt.Token).Claims.(*auth.JWTCustomClaims).ID

	result, err := BatchService.Get(userID)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, result)
}
