package batch

import (
	BatchService "bitbucket.org/batchy/api/v1/services/batch"
	"github.com/labstack/echo"
	"net/http"
)

func Update(c echo.Context) error {
	name := c.FormValue("name")
	description := c.FormValue("description")
	batchID := c.FormValue("batchId")

	err := BatchService.Update(name, description, batchID)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, "Successfully updated batch.")
}
