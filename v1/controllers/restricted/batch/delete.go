package batch

import (
	BatchService "bitbucket.org/batchy/api/v1/services/batch"
	"github.com/labstack/echo"
	"net/http"
)

func Delete(c echo.Context) error {
	batchID := c.FormValue("batchId")

	err := BatchService.Delete(batchID)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, "Successfully deleted batch.")
}
