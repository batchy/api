package batch

import (
	BatchService "bitbucket.org/batchy/api/v1/services/batch"
	"bitbucket.org/batchy/api/v1/utils/auth"
	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"
	"net/http"
)

func Create(c echo.Context) error {
	userID := c.Get("user").(*jwt.Token).Claims.(*auth.JWTCustomClaims).ID
	name := c.FormValue("name")
	description := c.FormValue("description")

	err := BatchService.Create(name, description, userID)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, "Successfully created batch.")
}
