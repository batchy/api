package subtask

import (
	SubtaskService "bitbucket.org/batchy/api/v1/services/subtask"
	"github.com/labstack/echo"
	"net/http"
)

func Delete(c echo.Context) error {
	id := c.FormValue("taskId")

	err := SubtaskService.Delete(id)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, "Successfully deleted task.")
}