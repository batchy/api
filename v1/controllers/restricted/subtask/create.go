package subtask

import (
	SubtaskService "bitbucket.org/batchy/api/v1/services/subtask"
	"github.com/labstack/echo"
	"net/http"
)

func Create(c echo.Context) error {
	body := c.FormValue("body")
	taskID := c.Param("taskID")

	err := SubtaskService.Create(body, taskID)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, "Successfully created subtask.")
}
