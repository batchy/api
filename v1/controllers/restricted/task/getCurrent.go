package task

import (
	TaskService "bitbucket.org/batchy/api/v1/services/task"
	"bitbucket.org/batchy/api/v1/utils/auth"
	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"
	"net/http"
)

func GetCurrent(c echo.Context) error {
	userID := c.Get("user").(*jwt.Token).Claims.(*auth.JWTCustomClaims).ID
	tasks, err := TaskService.GetCurrent(userID)
	if err != nil {
		return err
	}
	return c.JSON(http.StatusOK, tasks)
}
