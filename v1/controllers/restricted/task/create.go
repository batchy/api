package task

import (
	TaskService "bitbucket.org/batchy/api/v1/services/task"
	"bitbucket.org/batchy/api/v1/utils/auth"
	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"
	"net/http"
)

func Create(c echo.Context) error {
	userID := c.Get("user").(*jwt.Token).Claims.(*auth.JWTCustomClaims).ID
	title := c.FormValue("name")
	description := c.FormValue("description")
	batchID := c.FormValue("batchId")
	dueDate := c.FormValue("dueDate")

	err := TaskService.Create(userID, title, description, dueDate, batchID)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, "Successfully created task.")
}
