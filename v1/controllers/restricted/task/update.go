package task

import (
	TaskService "bitbucket.org/batchy/api/v1/services/task"
	"github.com/labstack/echo"
	"net/http"
)

func Update(c echo.Context) error {
	id := c.FormValue("taskId")
	name := c.FormValue("name")
	description := c.FormValue("description")
	status := c.FormValue("status")
	dueDate := c.FormValue("dueDate")
	batchID := c.FormValue("batchId")

	err := TaskService.Update(id, name, description, status, dueDate, batchID)
	if err != nil {
		return err
	}
	return c.JSON(http.StatusOK, "Successfully updated task.")
}
