package task

import (
	TaskService "bitbucket.org/batchy/api/v1/services/task"
	"github.com/labstack/echo"
	"net/http"
)

func Delete(c echo.Context) error {
	id := c.FormValue("taskId")

	err := TaskService.Delete(id)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, "Successfully deleted task.")
}
