package auth

import (
	AuthService "bitbucket.org/batchy/api/v1/services/auth"
	"github.com/labstack/echo"
	"net/http"
)

func Login(c echo.Context) error {
	username := c.FormValue("username")
	password := c.FormValue("password")

	user, token, err := AuthService.Login(username, password)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, echo.Map{"data": user, "token": token})
}
