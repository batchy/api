package batch

import (
	"bitbucket.org/batchy/api/v1/environment"
	"bitbucket.org/batchy/api/v1/utils/db"
	"github.com/labstack/echo"
	"net/http"
)

func Update(name string, description string, batchID string) *echo.HTTPError {
	if batchID == "" {
		return echo.NewHTTPError(http.StatusBadRequest, "Bad request: batch id is not provided.")
	}
	if name == "" {
		return echo.NewHTTPError(http.StatusBadRequest, "Bad request: name is not provided.")
	}

	stmt, err := environment.DB.Prepare(SQLUpdateBatch)
	if err != nil {
		return echo.NewHTTPError(http.StatusFailedDependency, err)
	}
	_, err = stmt.Exec(name, db.NewNullString(description), batchID)
	if err != nil {
		return echo.NewHTTPError(http.StatusFailedDependency, err)
	}

	return nil
}
