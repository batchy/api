package batch

import (
	"bitbucket.org/batchy/api/v1/environment"
	"github.com/labstack/echo"
	"net/http"
)

func Delete(batchID string) *echo.HTTPError {
	if batchID == "" {
		return echo.NewHTTPError(http.StatusBadRequest, "Bad request: no request provided")
	}

	stmt, err := environment.DB.Prepare(SQLDeleteBatch)
	if err != nil {
		return echo.NewHTTPError(http.StatusFailedDependency, err)
	}
	_, err = stmt.Exec(batchID)
	if err != nil {
		return echo.NewHTTPError(http.StatusFailedDependency, err)
	}

	return nil
}
