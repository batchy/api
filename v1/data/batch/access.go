package batch

import (
	"database/sql"
)

type SQLBatch struct {
	ID              string
	Name            string
	Description     sql.NullString
	TaskID          sql.NullString
	TaskName        sql.NullString
	TaskDescription sql.NullString
	TaskDueDate     sql.NullTime
	SubtaskID       sql.NullString
	SubtaskBody     sql.NullString
}

const (
	SQLCreateBatch = `INSERT INTO batches (id, name, description, user_id) VALUE (?, ?, ?, ?);`
	SQLGetBatches  = `
		SELECT
       		b.id,
       		b.name,
       		b.description,
       		t.id as task_id,
       		t.name as task_name,
       		t.description as task_description,
			t.due_date,
       		s.id as subtask_id,
       		s.body as subtask_body
		FROM batches AS b
		LEFT JOIN tasks t on b.id = t.batch_id AND t.status='current'
		LEFT JOIN subtasks s on t.id = s.task_id
		WHERE b.user_id = ?;
	`
	SQLUpdateBatch = `
		UPDATE batches
		SET name = ?,
    		description = ?
		WHERE id = ?;
	`
	SQLDeleteBatch = `DELETE FROM batches WHERE id = ?;`
)
