package batch

import (
	"bitbucket.org/batchy/api/v1/environment"
	"bitbucket.org/batchy/api/v1/utils/db"
	"github.com/labstack/echo"
	"net/http"
)

func Create(id string, name string, description string, userID string) *echo.HTTPError {
	if id == "" {
		return echo.NewHTTPError(http.StatusInternalServerError, "ISE: could not generate UUID for batch.")
	}
	if userID == "" {
		return echo.NewHTTPError(http.StatusInternalServerError, "ISE: invalid userID.")
	}
	if name == "" {
		return echo.NewHTTPError(http.StatusBadRequest, "Bad request: batch name is not provided.")
	}

	stmt, err := environment.DB.Prepare(SQLCreateBatch)
	if err != nil {
		return echo.NewHTTPError(http.StatusFailedDependency, err)
	}
	_, err = stmt.Exec(id, name, db.NewNullString(description), userID)
	if err != nil {
		return echo.NewHTTPError(http.StatusFailedDependency, err)
	}

	return nil
}
