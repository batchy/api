package batch

import (
	"bitbucket.org/batchy/api/v1/environment"
	"github.com/labstack/echo"
	"net/http"
)

func Get(userID string) ([]SQLBatch, *echo.HTTPError) {
	if userID == "" {
		return nil, echo.NewHTTPError(http.StatusInternalServerError, "ISE: invalid userId.")
	}

	var results []SQLBatch

	rows, err := environment.DB.Query(SQLGetBatches, userID)
	if err != nil {
		return nil, echo.NewHTTPError(http.StatusFailedDependency, err)
	}
	defer rows.Close()
	for rows.Next() {
		result := &SQLBatch{}

		err := rows.Scan(
			&result.ID,
			&result.Name,
			&result.Description,
			&result.TaskID,
			&result.TaskName,
			&result.TaskDescription,
			&result.TaskDueDate,
			&result.SubtaskID,
			&result.SubtaskBody,
		)
		if err != nil {
			return nil, echo.NewHTTPError(http.StatusFailedDependency, err)
		}
		results = append(results, *result)
	}
	return results, nil
}
