package task

import (
	"bitbucket.org/batchy/api/v1/environment"
	"github.com/labstack/echo"
	"net/http"
)

func GetCompleted(id string) ([]SQLTask, *echo.HTTPError) {
	var results []SQLTask

	rows, err := environment.DB.Query(SQLGetCompletedTasks, id)
	if err != nil {
		return nil, echo.NewHTTPError(http.StatusFailedDependency, err)
	}
	defer rows.Close()
	for rows.Next() {
		result := &SQLTask{}
		err := rows.Scan(&result.ID, &result.Name, &result.Description, &result.Status, &result.DueDate, &result.BatchID, &result.BatchName, &result.SubtaskID, &result.SubtaskBody)
		if err != nil {
			return nil, echo.NewHTTPError(http.StatusFailedDependency, err)
		}
		results = append(results, *result)
	}
	return results, nil
}
