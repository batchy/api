package task

import (
	"bitbucket.org/batchy/api/v1/environment"
	"bitbucket.org/batchy/api/v1/utils/db"
	"github.com/labstack/echo"
	"net/http"
	"time"
)

func Update(id string, name string, description string, status string, dueDate time.Time, batchID string) *echo.HTTPError {
	if id == "" {
		return echo.NewHTTPError(http.StatusBadRequest, "Bad request: task id is not provided.")
	}

	stmt, err := environment.DB.Prepare(SQLUpdateTask)
	if err != nil {
		return echo.NewHTTPError(http.StatusFailedDependency, err)
	}
	_, err = stmt.Exec(name, db.NewNullString(description), status, db.NewNullTime(dueDate), db.NewNullString(batchID), id)
	if err != nil {
		return echo.NewHTTPError(http.StatusFailedDependency, err)
	}
	return nil
}
