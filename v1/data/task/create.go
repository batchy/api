package task

import (
	"bitbucket.org/batchy/api/v1/environment"
	"bitbucket.org/batchy/api/v1/utils/db"
	"github.com/labstack/echo"
	"net/http"
	"time"
)

func Create(id string, name string, description string, dueDate time.Time, userID string, batchID string) *echo.HTTPError {
	if id == "" {
		return echo.NewHTTPError(http.StatusBadRequest, "Bad request: task id is not provided.")
	}
	if name == "" {
		return echo.NewHTTPError(http.StatusBadRequest, "Bad request: task title is not provided.")
	}

	stmt, err := environment.DB.Prepare(SQLCreateTask)
	if err != nil {
		return echo.NewHTTPError(http.StatusFailedDependency, err)
	}
	_, err = stmt.Exec(id, name, description, db.NewNullTime(dueDate), userID, db.NewNullString(batchID))
	if err != nil {
		return echo.NewHTTPError(http.StatusFailedDependency, err)
	}

	return nil
}