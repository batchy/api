package task

import "database/sql"

type SQLTask struct {
	ID          string
	Name        string
	Description sql.NullString
	Status      string
	DueDate     sql.NullTime
	BatchID     sql.NullString
	BatchName   sql.NullString
	SubtaskID   sql.NullString
	SubtaskBody sql.NullString
}

const (
	SQLGetCurrentTasks = `
		SELECT t.id, t.name, t.description, t.status, t.due_date, t.batch_id, b.name as batch_name, s.id AS subtask_id, s.body AS subtask_body
		FROM tasks AS t
		LEFT JOIN subtasks s on t.id = s.task_id
		LEFT JOIN batches b on t.batch_id = b.id
		WHERE t.user_id = ? AND t.status = 'current'
		ORDER BY t.created_at DESC
	`
	SQLGetCompletedTasks = `
		SELECT t.id, t.name, t.description, t.status, t.due_date, t.batch_id, b.name as batch_name, s.id AS subtask_id, s.body AS subtask_body
		FROM tasks AS t
		LEFT JOIN subtasks s on t.id = s.task_id
		LEFT JOIN batches b on t.batch_id = b.id
		WHERE t.user_id = ? AND t.status = 'completed'
		ORDER BY t.created_at DESC
	`
	SQLCreateTask = "INSERT into tasks(id, name, description, status, due_date, user_id, batch_id) VALUES(?, ?, ?, 'current', ?, ?, ?)"
	SQLUpdateTask = `
		UPDATE tasks 
		SET name = ?, description = ?, status = ?, due_date = ?, batch_id = ? 
		WHERE id = ?
	`
	SQLDeleteTask = `DELETE FROM tasks WHERE id = ?`
)
