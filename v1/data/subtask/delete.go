package subtask

import (
"bitbucket.org/batchy/api/v1/environment"
"github.com/labstack/echo"
"net/http"
)

func Delete(id string) *echo.HTTPError {
	if id == "" {
		return echo.NewHTTPError(http.StatusBadRequest, "Bad request: subtask id is not provided.")
	}

	stmt, err := environment.DB.Prepare(SQLDeleteSubtask)
	if err != nil {
		return echo.NewHTTPError(http.StatusFailedDependency, err)
	}
	_, err = stmt.Exec(id)
	if err != nil {
		return echo.NewHTTPError(http.StatusFailedDependency, err)
	}

	return nil
}