package subtask

import (
	"bitbucket.org/batchy/api/v1/environment"
	"github.com/labstack/echo"
	"net/http"
)

func Create(id string, body string, taskID string) *echo.HTTPError {
	if id == "" {
		return echo.NewHTTPError(http.StatusInternalServerError, "Bad request: subtask id is not provided.")
	}
	if body == "" {
		return echo.NewHTTPError(http.StatusBadRequest, "Bad request: subtask is not provided.")
	}

	stmt, err := environment.DB.Prepare(SQLCreateSubtask)
	if err != nil {
		return echo.NewHTTPError(http.StatusFailedDependency, err)
	}
	_, err = stmt.Exec(id, body, taskID)
	if err != nil {
		return echo.NewHTTPError(http.StatusFailedDependency, err)
	}

	return nil
}
