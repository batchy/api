package user

// SQLUser defines a model for an SQL representation of user.
type SQLUser struct {
	ID       string
	Username string
	Password string
}

const (
	SQLCreateUser = "INSERT into users(id, username, password) VALUES (?, ?, ?)"
	SQLGetUser    = "SELECT id, username, password FROM users WHERE users.username = ?"
)
