package user

import (
	"bitbucket.org/batchy/api/v1/environment"
	"github.com/labstack/echo"
	"net/http"
)

// Create adds the user to the database.
func Create(id string, username string, password string) *echo.HTTPError {
	if id == "" {
		return echo.NewHTTPError(http.StatusInternalServerError, "Could not generate user ID.")
	}
	if username == "" {
		return echo.NewHTTPError(http.StatusBadRequest, "Bad request: no username provided.")
	}
	if password == "" {
		return echo.NewHTTPError(http.StatusBadRequest, "Bad request: no password provided.")
	}

	stmt, err := environment.DB.Prepare(SQLCreateUser)
	if err != nil {
		return echo.NewHTTPError(http.StatusFailedDependency, err)
	}
	_, err = stmt.Exec(id, username, password)
	if err != nil {
		return echo.NewHTTPError(http.StatusFailedDependency, err)
	}

	return nil
}
