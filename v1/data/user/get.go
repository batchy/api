package user

import (
	"bitbucket.org/batchy/api/v1/environment"
	"github.com/labstack/echo"
	"net/http"
)

// Get fetches a user from the database.
func Get(username string) (*SQLUser, *echo.HTTPError) {
	if username == "" {
		return nil, echo.NewHTTPError(http.StatusBadRequest, "Bad request: no username provided.")
	}

	var result = &SQLUser{}
	err := environment.DB.QueryRow(SQLGetUser, username).Scan(&result.ID, &result.Username, &result.Password)
	if err != nil {
		return nil, echo.NewHTTPError(http.StatusFailedDependency, err)
	}

	return result, nil
}
