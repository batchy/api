package batch

import (
	"bitbucket.org/batchy/api/v1/data/batch"
	"bitbucket.org/batchy/api/v1/data/task"
	taskUtil "bitbucket.org/batchy/api/v1/utils/task"
	//"bitbucket.org/batchy/api/v1/data/task"
	"bitbucket.org/batchy/api/v1/models"
)

//AggregateOne transforms batch data and builds a batch model out of it.
func AggregateOne(data []batch.SQLBatch) *models.Batch {

	// empty batch
	batchModel := &models.Batch{}
	var batchTaskData []task.SQLTask // iterate over provided data
	for _, row := range data {
		// set id
		batchModel.ID = row.ID
		// set name
		batchModel.Name = row.Name
		// check if description is valid and set it if valid
		if row.Description.Valid {
			batchModel.Description = row.Description.String
		}

		if row.TaskID.Valid && row.TaskName.Valid {
			newTask := &task.SQLTask{}
			newTask.ID = row.TaskID.String
			newTask.Name = row.TaskName.String
			newTask.Description = row.TaskDescription
			newTask.DueDate = row.TaskDueDate
			newTask.SubtaskID = row.SubtaskID
			newTask.SubtaskBody = row.SubtaskBody
			batchTaskData = append(batchTaskData, *newTask)
		}
	}

	batchTasks := taskUtil.AggregateMany(batchTaskData)
	batchModel.Tasks = batchTasks

	return batchModel
}

// AggregateMany converts multiple tasks into a models.Task array.
func AggregateMany(data []batch.SQLBatch) []*models.Batch {
	var batches []*models.Batch
	batchDataMap := make(map[string][]batch.SQLBatch)

	// Iterate over data and sort it by batch.ID.
	var keys []string // use keys to preserve order when ranging over taskDataMap
	for _, batchData := range data {
		batchIsPresent := false
		for _, mapKey := range keys {
			if batchData.ID == mapKey {
				batchIsPresent = true
			}
		}
		if !batchIsPresent {
			keys = append(keys, batchData.ID)
		}
		batchDataMap[batchData.ID] = append(batchDataMap[batchData.ID], batchData)
	}
	//fmt.Println(keys)
	for _, key := range keys {
		batches = append(batches, AggregateOne(batchDataMap[key]))
	}

	return batches
}
