package auth

import (
	"bitbucket.org/batchy/api/v1/environment"
	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"
	"net/http"
	"time"
)

// DispatchToken creates a new JWT with an id in its body.
func DispatchToken(id string) (string, *echo.HTTPError) {
	claims := &JWTCustomClaims{
		id,
		jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Hour * 72).Unix(),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	t, err := token.SignedString([]byte(environment.SECRET))
	if err != nil {
		return "", echo.NewHTTPError(http.StatusInternalServerError, "Could not generate token.")
	}
	return t, nil
}
