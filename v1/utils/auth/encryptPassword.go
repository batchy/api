package auth

import (
	"github.com/labstack/echo"
	"golang.org/x/crypto/bcrypt"
	"net/http"
)

func EncryptPassword(password string) (string, *echo.HTTPError) {
	if password == "" {
		return "", echo.NewHTTPError(http.StatusBadRequest, "Bad request: password is not provided.")
	}
	hash, err := bcrypt.GenerateFromPassword([]byte(password), 10)
	if err != nil {
		return "", echo.NewHTTPError(http.StatusInternalServerError, err)
	}
	return string(hash), nil
}
