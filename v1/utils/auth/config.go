package auth

import (
	"bitbucket.org/batchy/api/v1/environment"
	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo/middleware"
)

type JWTCustomClaims struct {
	ID string `json:"id"`
	jwt.StandardClaims
}

var CustomJWTConfig = middleware.JWTConfig{
	Claims:     &JWTCustomClaims{},
	SigningKey: []byte(environment.SECRET),
}
