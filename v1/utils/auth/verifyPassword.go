package auth

import (
	"github.com/labstack/echo"
	"golang.org/x/crypto/bcrypt"
	"net/http"
)

func VerifyPassword(password []byte, hash []byte) *echo.HTTPError {
	err := bcrypt.CompareHashAndPassword(hash, password)
	if err != nil {
		return echo.NewHTTPError(http.StatusUnauthorized, err)
	}
	return nil
}
