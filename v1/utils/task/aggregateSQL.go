package task

import (
	"bitbucket.org/batchy/api/v1/data/task"
	"bitbucket.org/batchy/api/v1/models"
)

// AggregateOne converts task data and builds a task model out of it.
func AggregateOne(data []task.SQLTask) *models.Task {
	taskModel := &models.Task{}
	for _, row := range data {
		taskModel.ID = row.ID
		taskModel.Name = row.Name
		if row.Description.Valid {
			taskModel.Description = row.Description.String
		} else {
			taskModel.Description = ""
		}
		taskModel.Status = row.Status
		if row.DueDate.Valid {
			taskModel.DueDate = row.DueDate.Time
		}
		if row.BatchID.Valid {
			taskModel.BatchID = row.BatchID.String
		} else {
			taskModel.BatchID = ""
		}
		if row.BatchName.Valid {
			taskModel.BatchName = row.BatchName.String
		}
		if row.SubtaskID.Valid && row.SubtaskBody.Valid {
			taskModel.Subtasks = append(taskModel.Subtasks, models.Subtask{ID: row.SubtaskID.String, Body: row.SubtaskBody.String})
		}
	}
	return taskModel
}

// AggregateMany converts multiple tasks into a models.Task array.
func AggregateMany(data []task.SQLTask) []*models.Task {
	var tasks []*models.Task
	taskDataMap := make(map[string][]task.SQLTask)

	// Iterate over data and sort it by task.ID.
	var keys []string // use keys to preserve order when ranging over taskDataMap
	for _, row := range data {
		isPresent := false
		for _, mapKey := range keys {
			if row.ID == mapKey {
				isPresent = true
			}
		}
		if !isPresent {
			keys = append(keys, row.ID)
		}
		taskDataMap[row.ID] = append(taskDataMap[row.ID], row)
	}

	// Aggregate every task map into a task object and append it to the result array.
	for _, key := range keys {
		tasks = append(tasks, AggregateOne(taskDataMap[key]))
	}

	return tasks
}
