package batch

import (
	BatchDAO "bitbucket.org/batchy/api/v1/data/batch"
	"github.com/google/uuid"
	"github.com/labstack/echo"
)

func Create(name string, description string, userID string) *echo.HTTPError {
	id := uuid.New().String()

	err := BatchDAO.Create(id, name, description, userID)
	if err != nil {
		return err
	}

	return nil
}
