package batch

import (
	BatchDAO "bitbucket.org/batchy/api/v1/data/batch"
	"github.com/labstack/echo"
)

func Update(name string, description string, batchID string) *echo.HTTPError {
	err := BatchDAO.Update(name, description, batchID)
	if err != nil {
		return err
	}
	return nil
}
