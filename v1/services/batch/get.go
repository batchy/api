package batch

import (
	BatchDAO "bitbucket.org/batchy/api/v1/data/batch"
	"bitbucket.org/batchy/api/v1/models"
	"bitbucket.org/batchy/api/v1/utils/batch"
	"fmt"
	"github.com/labstack/echo"
)

func Get(userID string) ([]*models.Batch, *echo.HTTPError) {
	batchData, err := BatchDAO.Get(userID)
	if err != nil {
		return nil, err
	}
	result := batch.AggregateMany(batchData)
	fmt.Println(result)
	return result, nil
}
