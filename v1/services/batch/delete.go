package batch

import (
	BatchDAO "bitbucket.org/batchy/api/v1/data/batch"
	"github.com/labstack/echo"
)

func Delete(batchID string) *echo.HTTPError {
	err := BatchDAO.Delete(batchID)
	if err != nil {
		return err
	}

	return nil
}
