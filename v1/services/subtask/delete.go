package subtask

import (
	SubtaskDAO "bitbucket.org/batchy/api/v1/data/subtask"
	"github.com/labstack/echo"
)

func Delete(id string) *echo.HTTPError {
	err := SubtaskDAO.Delete(id)
	if err != nil {
		return err
	}

	return nil
}
