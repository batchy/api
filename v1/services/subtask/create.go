package subtask

import (
	SubtaskDAO "bitbucket.org/batchy/api/v1/data/subtask"
	"github.com/google/uuid"
	"github.com/labstack/echo"
)

func Create(body string, taskID string) *echo.HTTPError {
	id := uuid.New().String()
	err := SubtaskDAO.Create(id, body, taskID)
	if err != nil {
		return err
	}

	return nil
}