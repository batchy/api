package task

import (
	TaskDAO "bitbucket.org/batchy/api/v1/data/task"
	"github.com/labstack/echo"
)

func Delete(id string) *echo.HTTPError {
	err := TaskDAO.Delete(id)
	if err != nil {
		return err
	}

	return nil
}
