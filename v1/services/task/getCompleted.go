package task

import (
	userDAO "bitbucket.org/batchy/api/v1/data/task"
	"bitbucket.org/batchy/api/v1/models"
	"bitbucket.org/batchy/api/v1/utils/task"
	"github.com/labstack/echo"
)

func GetCompleted(userID string) ([]*models.Task, *echo.HTTPError) {
	taskData, err := userDAO.GetCompleted(userID)
	if err != nil {
		return nil, err
	}
	tasks := task.AggregateMany(taskData)
	return tasks, nil
}
