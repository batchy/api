package task

import (
	TaskDAO "bitbucket.org/batchy/api/v1/data/task"
	"bitbucket.org/batchy/api/v1/environment"
	"github.com/google/uuid"
	"github.com/labstack/echo"
	"time"
)

func Create(userID string, name string, description string, dueDate string, batchID string) *echo.HTTPError {
	taskID := uuid.New().String()
	parsedTime, _ := time.Parse(environment.TimeFormat, dueDate)
	err := TaskDAO.Create(taskID, name, description, parsedTime, userID, batchID)
	if err != nil {
		return err
	}

	return nil
}
