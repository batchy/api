package task

import (
	userDAO "bitbucket.org/batchy/api/v1/data/task"
	"bitbucket.org/batchy/api/v1/models"
	"bitbucket.org/batchy/api/v1/utils/task"
	"github.com/labstack/echo"
)

func GetCurrent(userID string) ([]*models.Task, *echo.HTTPError) {
	taskData, err := userDAO.GetCurrent(userID)
	if err != nil {
		return nil, err
	}
	tasks := task.AggregateMany(taskData)
	return tasks, nil
}

