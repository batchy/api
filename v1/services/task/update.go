package task

import (
	TaskDAO "bitbucket.org/batchy/api/v1/data/task"
	"bitbucket.org/batchy/api/v1/environment"
	"github.com/labstack/echo"
	"time"
)

func Update(id string, name string, description string, status string, dueDate string, batchID string) *echo.HTTPError {
	parsedTime, _ := time.Parse(environment.TimeFormat, dueDate)

	err := TaskDAO.Update(id, name, description, status, parsedTime, batchID)
	if err != nil {
		return err
	}
	return nil
}
