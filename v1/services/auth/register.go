package auth

import (
	userDAO "bitbucket.org/batchy/api/v1/data/user"
	"bitbucket.org/batchy/api/v1/utils/auth"
	"github.com/google/uuid"
	"github.com/labstack/echo"
)

func Register(username string, password string) (string, *echo.HTTPError) {
	hashedPassword, err := auth.EncryptPassword(password)
	if err != nil {
		return "", err
	}

	// Generate new uuid
	id := uuid.New().String()

	// Create a new user
	err = userDAO.Create(id, username, hashedPassword)
	if err != nil {
		return "", err
	}

	// Generate a JWT based on user id
	token, err := auth.DispatchToken(id)
	if err != nil {
		return "", err
	}

	return token, nil
}
