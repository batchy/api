package auth

import (
	userDAO "bitbucket.org/batchy/api/v1/data/user"
	"bitbucket.org/batchy/api/v1/models"
	"bitbucket.org/batchy/api/v1/utils/auth"
	"github.com/labstack/echo"
)

func Login(username string, password string) (*models.User, string, *echo.HTTPError) {
	userData, err := userDAO.Get(username)
	if err != nil {
		return nil, "", err
	}

	// Verify password hash
	hash := userData.Password
	err = auth.VerifyPassword([]byte(password), []byte(hash))
	if err != nil {
		return nil, "", err
	}

	// Generate user
	user := &models.User{ID: userData.ID, Username: userData.Username}

	// Generate a JWT based on user id
	token, err := auth.DispatchToken(user.ID)
	if err != nil {
		return nil, "", err
	}

	return user, token, nil
}
