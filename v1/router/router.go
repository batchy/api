package router

import (
	"bitbucket.org/batchy/api/v1/controllers/public/auth"
	"bitbucket.org/batchy/api/v1/controllers/restricted/batch"
	"bitbucket.org/batchy/api/v1/controllers/restricted/subtask"
	"bitbucket.org/batchy/api/v1/controllers/restricted/task"
	authConfig "bitbucket.org/batchy/api/v1/utils/auth"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

// Router creates a new instance of echo with attached middleware.
// It returns a pointer to a new instance of Echo.
func Init() *echo.Echo {
	e := echo.New()
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{echo.GET, echo.HEAD, echo.PUT, echo.POST, echo.DELETE},
	}))

	// Public group
	p := e.Group("/public")
	p.POST("/register", auth.Register)
	p.POST("/login", auth.Login)

	// Restricted group
	r := e.Group("/restricted")
	config := authConfig.CustomJWTConfig
	r.Use(middleware.JWTWithConfig(config))

	r.POST("/task", task.Create)
	r.GET("/tasks/completed", task.GetCompleted)
	r.GET("/tasks/current", task.GetCurrent)
	r.PUT("/task", task.Update)
	r.DELETE("/task", task.Delete)

	r.POST("/task/:taskID/subtask", subtask.Create)
	r.DELETE("/subtask", subtask.Delete)

	r.POST("/batch", batch.Create)
	r.GET("/batch", batch.Get)
	r.PUT("/batch", batch.Update)
	r.DELETE("/batch", batch.Delete)

	return e
}
